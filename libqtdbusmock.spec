


Name:		libqtdbusmock
Version:	r122
Release:	1%{?dist}
Summary:	Library for mocking DBus interactions using Qt
License:	LGPL-v3
Group:		System/Libraries
URL:		https://github.com/ubports/libqtdbusmock
Source:		https://github.com/abhishek9650/libqtdbusmock/archive/%{name}-%{version}.tar.gz
	

BuildRequires:	cmake, cmake-extras, gmock-devel, libqtdbustest, gtest-devel
BuildRequires:	pkgconfig(NetworkManager)
Requires:	gmock, libqtdbustest, networkmanager


%description
Library for mocking DBus interactions using Qt.

%package devel
Summary:	Library for mocking DBus interactions using Qt -- Development Libraries
Group:		Development/Libraries
Requires:	%{name} = %{version}

%description devel
Library for mocking DBus interactions using Qt
This package provides the development files for %{name}

%prep
%setup -q


%build
%cmake	-DCMAKE_INSTALL_PREFIX:PATH=%{_prefix}   \
	-DCMAKE_INSTALL_LIBDIR=%{_libdir}
make %{?_smp_mflags}

%install
%make_install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%{_libdir}/libqtdbusmock.so.1
%{_libdir}/libqtdbusmock.so.1.0.0

%files devel
%license COPYING
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}-1.pc
%{_includedir}/%{name}-1/%{name}/*.h
##This should go in a separate python package
%{_datadir}/%{name}/templates/*.py
%{_datadir}/%{name}/templates/*.pyc
%{_datadir}/%{name}/templates/*.pyo

%changelog

